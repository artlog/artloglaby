#!/bin/bash

findmainjar()
{
    for MJ in $(find . -name "*.jar")
    do
	echo "$MJ"
	if [[ $MJ =~ artloglaby-(.*)\.jar ]]
	then
	    VERSION=${BASH_REMATCH[1]}
	    echo "found $MJ version $VERSION"
	    MAINJAR=$MJ
	fi
    done
    
}

echo "Ce script a été conçu pour une distribution linux"
echo "This script was created for linux"

findmainjar

if [[ -z $MAINJAR ]]
then
    echo "[ERREUR] aucun jar artloglaby-xxx.jar dans ce répertoire"    
    exit 1
fi

if which java
then
    java -jar $MAINJAR
else
    echo "Aucun java/jre de trouvé"
    echo
    echo "Pour ubuntu :"
    echo "sudo apt install openjdk-8-jre"
fi
