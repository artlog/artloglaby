import sys
import bpy

argv = sys.argv
argv = argv[argv.index("--") + 1:]  # get all args after "--"

bpy.ops.import_mesh.stl(filepath=argv[0], filter_glob="*.stl", files="", directory="")

