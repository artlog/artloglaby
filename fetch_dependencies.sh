#!/bin/bash


fetch_jackson_databind()
{
    libversion_jackson_databind=2.12.0
    lib_jackson_databind=jackson-databind-${libversion_jackson_databind}.jar
    if [[ ! -e libs/$lib_jackson_databind ]]
    then
	jacksonmaven=https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/${libversion_jackson_databind}/$lib_jackson_databind
	pushd libs
	wget $jacksonmaven
	popd
    fi    
}

fetch_zstd-jni()
{

    echo "compiled from https://github.com/luben/zstd-jni commit 54d3d50c16d96bd8a30e2d4c0a9648001a52d6f9"
    # had to hack through jar generation ...
    # cp ~/artisanlogiciel/ext_projects/java/zstd-jni/target/zstd-jni-1.5.2-2.jar .
    libversion_zstdjni=1.5.2-2
    pushd libs    
    wget https://repo1.maven.org/maven2/com/github/luben/zstd-jni/${libversion_zstdjni}/zstd-jni-${libversion_zstdjni}.jar
    popd
}

if [[ ! -d ../sharedrawweb ]]
then
    echo "[ERROR] expected a sharedrawweb project parent ( for exports )" >&2
fi

if [[ ! -d libs ]]
then
    mkdir libs
fi

libversion_artgaphic=0.2.0
lib_artgaphic=artgaphics-${libversion_artgaphic}.jar
if [[ ! -e libs/$lib_artgaphic ]]
then
    ln -s ../../sharedrawweb/dist/lib/$lib_artgaphic libs/
fi

if [[ -n $use_jackson ]]
then    
    fetch_jackson_databind
fi

fetch_zstd-jni
