#!/bin/bash

if [[ ! -f configure.ac ]]
then
    echo "[ERROR] Missing configure.ac . Please create one " >&2
    exit 1
fi

if which autoreconf
then

    echo "bootstrap using automake tools"
    echo "from configure.ac will generate configure"

    autoreconf --install

    echo "lauch ./configure && make"

else

    echo "[ERROR] Missing autoreconf script from autotools. Please install autotools ( package autotools-dev and autoreconf" >&2

fi
