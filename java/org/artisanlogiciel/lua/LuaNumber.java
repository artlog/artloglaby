package org.artisanlogiciel.lua;

public class LuaNumber
extends LuaObject {

    int number;

    LuaNumber(int number)
    {
        this.number = number;
    }

    @Override
    public String toString() {
        return "" + number ;
    }

    @Override
    public Object wrapToJava() {
        return new Integer(number);
    }
}
