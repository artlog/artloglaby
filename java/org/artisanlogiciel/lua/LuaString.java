package org.artisanlogiciel.lua;

public class LuaString
    extends LuaObject
{
    String string;

    public LuaString(StringBuffer stringBuffer)
    {
        string = stringBuffer.toString();
    }

    @Override
    public Object wrapToJava() {
        return string;
    }

    @Override
    public String toString() {
        return '"' + string + '"' ;
    }
}
