package org.artisanlogiciel.lua;

import org.artisanlogiciel.games.minetest.World;

import java.util.ArrayList;
import java.util.List;

public class Parser {

    LuaTuple currentTuple = null;
    List<String> errors = null;
    LuaSequence currentSequence = null;
    LuaObject lastObject = null;
    CharProvider provider;

    public Parser(CharProvider provider) {
        this.provider = provider;
    }

    Parser(Parser parent)
    {
        this.provider = parent.provider;
    }

    void startSequence()
    {
        currentSequence = new LuaSequence();
    }

    char getNextchar()
    {
        char c = provider.getNextchar();
        System.out.print(c);
        return c;
    }

    void pushBackChar(char c)
    {
        provider.pushBackChar(c);
    }

    LuaString parseString()
    {
        char c = 0;
        StringBuffer buffer = new StringBuffer();
        while ( (c = getNextchar()) != 0 )
        {
            switch (c)
            {
                case '"':
                    return new LuaString(buffer);
                default:
                    buffer.append(c);
                    // addChar
            }
        }
        return new LuaString(buffer);
    }

    private void error(char c, String info)
    {
        pushBackChar(c);
        if ( errors == null)
        {
            errors = new ArrayList();
        }
        System.err.println(info);
        errors.add(info);
    }

    LuaObject parseKey()
    {
        LuaString string = null;
        char c = 0;
        while ( (c = getNextchar()) != 0 )
        {
            switch (c)
            {
                case '"':
                    if ( string == null ) {
                        string = parseString();
                    }
                    else
                    {
                        error(c,"unexepected second string for a key");
                    }
                    break;
                case ']':
                    return string;
                default:
                    error(c,"unexpected at end of a '[' key");
                    // error...
            }
        }

        return string;
    }

    LuaSequence parseSequence()
    {
        Parser subparser = new Parser(this);
        subparser.startSequence();
        subparser.parse();

        return subparser.currentSequence;
    }

    LuaNumber parseNumber(char c)
    {
        int number = ( c - '0' );
        while ( (c = getNextchar()) != 0 ) {
            switch (c) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    number = number * 10 + ( c - '0' );
                    break;
                default:
                    pushBackChar(c);
                    return new LuaNumber(number);
            }
        }
        return new LuaNumber(number);
    }

    public LuaObject parse()
    {
        char c = 0;
        while ( ((c = getNextchar()) != 0 ) && ( errors == null ))
        {
            switch (c)
            {
                case '"':
                    LuaString string = parseString();
                    lastObject = string;
                    if ( currentSequence == null )
                    {
                        return string;
                    }
                    break ;
                case '[':
                    LuaObject key = parseKey();
                    currentTuple = new LuaTuple();
                    currentTuple.addItem(key);
                    break ;
                case ']':
                    // error ?
                    error(c,"unexpected end of key");
                    break ;
                case '=':
                    if ( currentTuple != null)
                    {
                        Parser subparser = new Parser(this);
                        // first item should be a key ...
                        currentTuple.addItem(subparser.parse());
                    }
                    else
                    {
                        error(c,"expected left part to be a key");
                    }
                    lastObject = currentTuple;
                    if ( currentSequence == null )
                    {
                        return currentTuple;
                    }
                    break ;
                case '{':
                    lastObject = parseSequence();
                    break ;
                case '}':
                    if ( currentSequence != null ) {
                        currentSequence.addObject(lastObject);
                    }
                    return currentSequence;
                case ',':
                    if ( currentSequence != null ) {
                        currentSequence.addObject(lastObject);
                        currentTuple = null;
                    }
                    break ;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    LuaNumber number = parseNumber(c);
                    lastObject = number;
                    if ( currentSequence == null )
                    {
                        return number;
                    }
                    break ;
                default:
                    error(c, "unexpected not a token or separator");
            }
        }
        if ( currentSequence == null )
        {
            System.out.println("null sequence");
        }
        return lastObject;
    }

    public static void main(String pArgs[])
    {
        CharProvider reader = new CharProvider(
                "{{[\"x\"]=2,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=1,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=1,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=24,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=24,[\"y\"]=0,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=2,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=3,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=5,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=5,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=5,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=24,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=24,[\"y\"]=4,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=6,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=7,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=8,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=9,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=10,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=11,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=12,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=13,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=24,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=24,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=14,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=15,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=16,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=17,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=19,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=19,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=18,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=19,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=21,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=2,[\"y\"]=21,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=10,[\"y\"]=21,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=21,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=14,[\"y\"]=21,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=16,[\"y\"]=21,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=20,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=0,[\"y\"]=23,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=6,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=4,[\"y\"]=23,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=8,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=12,[\"y\"]=23,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=18,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=22,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=22,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"},{[\"x\"]=20,[\"y\"]=23,[\"z\"]=0,[\"name\"]=\"default:dirt_with_grass\"}}"
        );
        Parser parser = new Parser(reader);
        LuaObject result = parser.parse();
        if ( result != null )
        {
            System.out.println(result.toString());
            Object we = result.wrapToJava();
            System.out.println(we);
            World world = new World();
            world.addList( (List<Object>) we);
            System.out.println(world);
        }
        else
        {
            System.err.println("result null");
        }
    }
}
