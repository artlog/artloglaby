package org.artisanlogiciel.lua;

public class CharProvider {

    String input;
    int current;
    int last;

    public CharProvider(String input) {
        this.input = input;
        current = 0;
    }

    public char getNextchar()
    {
        int i = current;
        int max = input.length();
        if ( max > current) {
            char c = 0;
            last = current;
            while ( ( max > i ) && ( c = input.charAt(i) ) == ' ' )
            {
                i++;
            }
            current = i + 1;
            return c;
        }
        else
        {
            return 0;
        }
    }

    public void pushBackChar(char c)
    {
        System.out.print('*');
        current = last;
    }

}
