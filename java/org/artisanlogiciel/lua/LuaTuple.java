package org.artisanlogiciel.lua;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LuaTuple
    extends  LuaObject
{
    List<LuaObject> items;


    public LuaTuple() {
        this.items = new ArrayList<>(2);
    }

    public LuaObject addItem(LuaObject item)
    {
        items.add(item);
        return item;
    }

    public String addInMap(HashMap<String,Object> map)
    {
        String key = (String) items.get(0).wrapToJava();
        Object value = items.get(1).wrapToJava();

        map.put(key,value);

        return key;
    }

    @Override
    public String toString() {
        StringBuffer buffer  = new StringBuffer();
        buffer.append('[');
        buffer.append(items.get(0).toString());
        buffer.append("]");
        buffer.append('=');
        buffer.append(items.get(1).toString());
        return buffer.toString();
    }
}
