package org.artisanlogiciel.games.maze;

public class XYGridIterator {


    Position min;
    Position max;

    Position next;

    public XYGridIterator(Position min, Position max) {
        this.min = min;
        this.max = max;
        reset();
    }

    public void reset()
    {
        this.next = new Position(min);
    }

    public boolean hasNext()
    {
        return next != null;
    }

    public Position next()
    {
        if ( next != null)
        {
            Position current = new Position(next);
            next.doTranslate(Position.stepX);
            if ( next.getX() >= max.getX()) {
                if (next.getY() < max.getY() - 1) {
                    next.setX(min.getX()).doTranslate(Position.stepY);
                } else {
                    next = null;
                }
            }
            return current;
        }
        return null;
    }
}
