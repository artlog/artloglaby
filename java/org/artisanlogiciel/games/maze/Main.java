package org.artisanlogiciel.games.maze;

import org.artisanlogiciel.games.maze.solve.SolvingModel;

import java.util.LinkedList;
import java.util.Scanner;

public class Main
extends Maze {
    public Main(LabyModel model) {
        super(model);
    }

    public MazeParamEditor editor() {
        MazeParamEditor editor = new MazeParamEditor(null);
        System.out.println("enter width height and maxdepth");
        Scanner console = new Scanner(System.in);
        editor.read(console);
        return editor;
    }

    public LabyMap generate2(MazeParamEditor params) {
        params.setSeed(1024L);
        SolvingModel model = new SolvingModel(params);
        model.generateWithEntry(0, 0);
        setModel(model);

        final int width = params.getWidth();
        final int height = params.getHeight();
        LinkedList<Position> exits = new LinkedList<Position>();
        model.addEntryOrExit(-1, 0);
        model.addEntryOrExit(width, height - 1);
        System.out.println(model.toLabyMap().toString());
        if (!model.check()) {
            System.out.println("Check failed");
        }
        model.debugOut();
        model.resolve(width - 1, height - 1, null);
        LabyMap labyMap = model.toLabyMap();
        return labyMap;
    }

    public static void main(String pArgs[]) {
        Main m = new Main(null);
        MazeParamEditor editor = m.editor();
        LabyMap map = m.generate2(editor);
        System.out.println(map.toString());
        //  ­_L|âJU¨©=[ªM]O
    }
}
