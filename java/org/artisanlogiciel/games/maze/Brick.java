package org.artisanlogiciel.games.maze;

/*

2x2 Tile to represent a labyrinth position with some walls

this is 2x2 downright most part of 3x3 centered tile.

center right
down   downright

ab
cd

a is 'H' or '.' or ' '
bcd is within 'H' or ' '

*/
public class Brick
{

    // order matters index is same as bits order
    public static final Position[] AllMoves = {
            new Position(0,0).doReverseTranslate(Position.stepX),
            new Position(Position.stepY),
            new Position(Position.stepX),
            new Position(0,0).doReverseTranslate(Position.stepY),
            // last is no move
            new Position(0,0)
    };

    // flag is in Brick settings
    static int getDirectionIndex(short flag)
    {
        // short a = (short) (flag >> FLAGLENGTH);
        for (int index = 0; index < AllMoves.length - 1; index ++) {
            if (isFlagSet((short) (1 << index), flag)) {
                return index;
            }
        }
        // return last index which is no move.
        return AllMoves.length - 1;
    }

    public static Position getDirectionMove(short direction) {
        return AllMoves[getDirectionIndex(direction)];
    }


    public final static short LEFT = 1;
    public final static short DOWN = 2;
    public final static short RIGHT = 4;
    public final static short UP = 8;
    public final static short ENTRY = 16;
    public final static short GOAL = 32;

    // TODO get rid of those chars
    char a;
    char b;
    char c;
    char d;

    short walls; // according to LabyModel.getWalls();

    protected Brick()
    {
        //
    }

    public Brick(String up, String low, short walls)
    {
        a = up.charAt(0);
        b = up.charAt(1);
        c = low.charAt(0);
        d = low.charAt(1);
        this.walls = walls;
    }

    public static boolean isFlagSet(short flags, short wall)
    {
        return ( flags & wall ) == flags;
    }

    public String getUpString()
    {
        return "" + a + b;
    }

    public String getLowString()
    {
        return "" + c + d;
    }

    public short getWalls()
    {
        return walls;
    }
}
