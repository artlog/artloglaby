package org.artisanlogiciel.games.maze;

import org.artisanlogiciel.games.maze.model.WidthHeightProvider;

/**
 * Get model represented by possible moves
 */
public interface MovesProvider
    extends WidthHeightProvider
{

    /** return possible moves from this position */
    short getMoves(int x, int y);

}
