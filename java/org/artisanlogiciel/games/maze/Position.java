package org.artisanlogiciel.games.maze;

/** position of a cell with maze */
public class Position
{
    public final static Position stepX = new Position(1,0);
    public final static Position stepY = new Position( 0,1);

    private int x, y;

    public Position(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Position(Position other)
    {
        this.x = other.x;
        this.y = other.y;
    }


    public int getX()
    {
        return this.x;
    }

    public int getY()
    {
        return this.y;
    }

    public Position translate(Position other) {
        return new Position(x + other.x, y + other.y);
    }

    public Position doTranslate(Position other) {
        x += other.x;
        y += other.y;
        return this;
    }

    public Position doReverseTranslate(Position other) {
        x -= other.x;
        y -= other.y;
        return this;
    }


    public void limitToMax(int mx, int my)
    {
        x = Math.min(x,mx);
        y = Math.min(y,my);
    }

    public void limitToMin(int mx, int my)
    {
        x = Math.max(x,mx);
        y = Math.max(y,my);
    }

    public Position setX(int x)
    {
        this.x = x;
        return this;
    }

    public Position setY(int y)
    {
        this.y = y;
        return this;
    }


    public String toString()
    {
        return "(" + x + "," + y + ")";
    }

    public boolean equals(Object other)
    {
        // disregards depth
        if (other instanceof Position )
            {
            Position p = (Position) other;
            return (p.getX() == x) && (p.getY() == y);
            }
        else
            {
            return false;
            }
    }

    /** WARNING [(a,b),(width,height)[ */
    public boolean within(int a, int b, int width, int height) {
        return ((x >= a) && (x < width) && (y >= b) && (y < height));
    }
}
