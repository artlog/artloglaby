package org.artisanlogiciel.games.maze;

import org.artisanlogiciel.games.maze.model.WidthHeightProvider;

/**
 * WallsProvider provide a Walls representation
 **/
public interface WallsProvider
    extends WidthHeightProvider
{
    /**
     * See Brick
     * 
     * Will set bits :
     * 3 2 1 0
     * (8)(4)(2)(1)
     * ^ > v <
     * U R D L
     * p i o e
     *   g w f
     *   h n t
     *   t
     **/
    short getWalls(int x, int y);

    short getWalls(Position cell);
}
