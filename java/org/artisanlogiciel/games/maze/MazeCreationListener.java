package org.artisanlogiciel.games.maze;

/**
 * MazeCreationListener
 **/
public interface MazeCreationListener
{
    void changed(Position cornerleft, Position cornerright, WallsProvider provider);
}
