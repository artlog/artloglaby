package org.artisanlogiciel.games.maze;

import java.io.File;
import java.util.Scanner;

/**
 MazeParamEditor to edit Maze Parameters ( current impl in console / Scanner )
 **/
class MazeParamEditor implements MazeParams
{
    long seed;
    int width;
    int height;
    int maxdepth;
    File labdir;
    String name;

    public MazeParamEditor(File saveDir)
    {
	name = null;
	labdir = saveDir;
    }

    public void read(Scanner console)
    {
	width = console.nextInt();
	height = console.nextInt();
	maxdepth = console.nextInt();
    }

    public long getSeed()
    {
        return seed;
    }

    public int getWidth()
    {
	return width;
    }

    public int getHeight()
    {
	return height;
    }

    public int getMaxDepth()
    {
	return maxdepth;
    }
    
    public String getName()
    {
	if (name == null)
            {
                name = "lab" + width + "x" + height;
            }
	return name;
    }

    public void setSeed(long seed)
    {
        this.seed = seed;
    }

    public File getSaveDir()
    {
	return labdir;
    }
}

