package org.artisanlogiciel.games.maze;

import org.artisanlogiciel.games.minetest.Range;

import java.util.HashMap;

public class LabyLayers
extends Range
{
    HashMap<Integer,LabyModel> layers = new HashMap<>();

    public LabyLayers()
    {
        super();
    }

    /** sub layers **/
    public LabyLayers(LabyLayers pLayers, Range pRange)
    {
        for ( int layer = pRange.getMin(); layer <= pRange.getMax(); layer ++)
        {
            addLabyModel(layer, pLayers.getLayer(layer));
        }
    }

    public void addLabyModel(int z, LabyModel model)
    {
        updateBounds(z);
        layers.put(new Integer(z), model);
    }

    public LabyModel getLayer(int z)
    {
        return layers.get(new Integer(z));
    }
}
