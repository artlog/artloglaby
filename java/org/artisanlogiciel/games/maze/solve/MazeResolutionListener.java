package org.artisanlogiciel.games.maze.solve;

import java.util.LinkedList;

/**
 * MazeResolutionListener used as interface between resolver and (mostly) GUI
 **/
public interface MazeResolutionListener
{

    boolean notifySearch(DirectionPosition pPosition);

    /* @deprecated
    use notifySearchInfoLevel(0, String pInfo);
     */
    void notifySearchError(String error);

    void notifySearchInfoLevel(int infoLevel, String pInfo);

    void notifyCompletion(LinkedList<DirectionPosition> solvedPath);

}
