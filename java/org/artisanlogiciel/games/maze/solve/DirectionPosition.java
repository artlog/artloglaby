package org.artisanlogiciel.games.maze.solve;

import org.artisanlogiciel.games.maze.LabyModel;
import org.artisanlogiciel.games.maze.Position;

/**
 * DirectionPosition
 * <p>
 * record direction and position ( direction as defined in LabyModel ).
 **/
public class DirectionPosition {
    private short direction;
    private Position position;

    // (direction,position)
    public DirectionPosition(short d, Position p) {
        direction = d;
        position = p;
    }

    public short getDirection() {
        return direction;
    }

    public void setDirection(short d) {
        direction = d;
    }

    public Position getPosition() {
        return position;
    }

    public String toString() {
        if (position != null) {
            return position.toString();
        }
        return "?";
    }

    // create a new DirectionPosition from this using one possible direction.
    public DirectionPosition moveToAdjacentDirection() {
        short pointingdirection = 0;
        Position p = null;
        if (LabyModel.isFlagSet(direction, LabyModel.RIGHT)) {
            p = new Position(position).doTranslate(Position.stepX);
            pointingdirection |= LabyModel.LEFT;
        } else if (LabyModel.isFlagSet(direction, LabyModel.LEFT)) {
            p = new Position(position).doReverseTranslate(Position.stepX);
            pointingdirection |= LabyModel.RIGHT;
        } else if (LabyModel.isFlagSet(direction, LabyModel.UP)) {
            p = new Position(position).doReverseTranslate(Position.stepY);
            pointingdirection |= LabyModel.DOWN;
        } else if (LabyModel.isFlagSet(direction, LabyModel.DOWN)) {
            p = new Position(position).doTranslate(Position.stepY);
            pointingdirection |= LabyModel.UP;
        } else {
            p = position;
        }

        return new DirectionPosition((short) pointingdirection, p);
    }

}
