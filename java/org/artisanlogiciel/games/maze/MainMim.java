package org.artisanlogiciel.games.maze;

import org.artisanlogiciel.games.minetest.net.MiM;

import java.net.InetSocketAddress;

public class MainMim {

    void minetestMime(String serverName, int port) {
        MiM mim = new MiM(30002, new InetSocketAddress(serverName, port));
        mim.launch();
    }

    public static void main(String pArgs[])
    {
        if ( pArgs.length > 1)
        {
            new MainMim().minetestMime(pArgs[0], Integer.parseInt(pArgs[1]));
        }
        else
        {
            System.err.println("[ERROR] please set minetest server hostname and port as arguments");
        }
    }
}

