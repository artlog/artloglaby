package org.artisanlogiciel.games.maze;

import static org.artisanlogiciel.games.maze.Brick.*;

public class BrickTextMapping {

    private String charmap;

    public BrickTextMapping(String pCharMap)
    {
        charmap = pCharMap;
    }

    public static String getDefaultDirLine()
    {
        char dir[] = new char[16];
        String s = "";

        /*
         * dir[LEFT | DOWN | RIGHT | UP]='O'; dir[LEFT | DOWN | RIGHT]='U';
         * dir[LEFT | UP | RIGHT]='M'; dir[LEFT | UP | DOWN]='['; dir[RIGHT | UP
         * | DOWN]=']'; dir[UP | DOWN]='='; dir[LEFT | RIGHT]='|'; dir[RIGHT |
         * DOWN]='J'; dir[LEFT | DOWN]='L'; dir [LEFT | UP]='T'; dir[UP |
         * RIGHT]='7'; dir[LEFT] = '!'; dir[RIGHT] ='|'; dir[DOWN]= '_';
         * dir[UP]= '¨'; dir[0]=' ';
         */

        dir[LEFT | DOWN | RIGHT | UP] = 'O';
        dir[LEFT | DOWN | RIGHT] = 'U';
        dir[LEFT | UP | RIGHT] = 'M';
        dir[LEFT | UP | DOWN] = '[';
        dir[RIGHT | UP | DOWN] = ']';
        dir[UP | DOWN] = '=';
        dir[LEFT | RIGHT] = 226;
        dir[RIGHT | DOWN] = 'J';
        dir[LEFT | DOWN] = 'L';
        dir[LEFT | UP] = 169;
        dir[UP | RIGHT] = 170;
        dir[LEFT] = 173;
        dir[RIGHT] = '|';
        dir[DOWN] = '_';
        dir[UP] = '¨';
        dir[0] = ' ';

        for (int i = 0; i < 16; i++)
        {
            s = s + dir[i];
        }

        return s;

    }

    public static char getDefaultChar(short walls)
    {
        // return getDirLine().charAt(walls & 0xFFF0);
        return getDefaultDirLine().charAt(walls);
    }

    public static char getDefaultChar(Brick brick)
    {
        // return getDirLine().charAt(walls & 0xFFF0);
        return getDefaultChar(brick.walls);
    }

    public char getChar(Brick brick)
    {
        return charmap.charAt((brick.walls)*0xf);
    }


}
