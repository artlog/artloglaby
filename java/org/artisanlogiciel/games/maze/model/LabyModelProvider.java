package org.artisanlogiciel.games.maze.model;

import org.artisanlogiciel.games.maze.MovesProvider;
import org.artisanlogiciel.games.maze.Position;
import org.artisanlogiciel.games.maze.WallsProvider;

public interface LabyModelProvider
extends WallsProvider,
        MovesProvider
{
    /**
     * add a new direction(s) exiting ones are kept
     */
    void addDirection(Position cell, short path);

    /* set direction(s) existing ones are lost */
    void setDirection(Position cell, short path);

    /**
     * is there no wall in that direction ?
     * WARNING direction is in Brick ( ex Brick.UP )
     **/
    boolean canMoveInDirection(Position where, short direction);

    /**
     * return if position position was change
     * WARNING direction is in Brick ( ex Brick.UP )
     *
     * @param inoutposition
     * @param direction
     * @return
     */
    boolean moveInDirection(Position inoutposition, short direction);

    /** like getMoves but include resolved information */
    short getPath(int x, int y);

}
