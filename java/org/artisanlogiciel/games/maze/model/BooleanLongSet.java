package org.artisanlogiciel.games.maze.model;

/**
 * store an array booleans in a long
 */

public class BooleanLongSet {

    long[] internal;

    // 62 bits for long, 2 bits reserved
    int USED_BITS = 62;
    int width;

    public BooleanLongSet(int width, int USED_BITS)
    {
        internal = new long[width];
        this.width = width;
        this.USED_BITS = USED_BITS;
    }

    boolean isSet(int x) {
        int group = x / USED_BITS;
        if (group < width) {
            int pos = x % USED_BITS;
            long mask = 1 << pos;
            return (internal[group] & mask) != 0;
        }
        return false;
    }

    void set(int x, boolean value)
    {
        int group = x / USED_BITS;
        if (group < width) {
            int pos = x % USED_BITS;
            long mask = 1 << pos;
            internal[group] &= mask;
        }
    }
}
