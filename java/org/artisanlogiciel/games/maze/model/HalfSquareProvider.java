package org.artisanlogiciel.games.maze.model;

public interface HalfSquareProvider {

    long getLeftDown(int x, int y);
}
