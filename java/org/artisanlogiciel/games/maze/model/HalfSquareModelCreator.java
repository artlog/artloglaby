package org.artisanlogiciel.games.maze.model;

import org.artisanlogiciel.games.maze.MovesProvider;
import org.artisanlogiciel.games.maze.WallsProvider;

public class HalfSquareModelCreator {

    interface SetXY
    {
        void setXY(HalfSquareRasterModel model, int x, int y);
    }

    HalfSquareRasterModel createFromFunc(int width, int height, SetXY func)
    {
        HalfSquareRasterModel model = new HalfSquareRasterModel(width,height);
        for (int y=0; y<model.getHeight();y++)
        {
            for (int x=0; x<model.getWidth();x++)
            {
                func.setXY(model,x,y);
            }
        }
        return model;
    }

    public HalfSquareRasterModel createFromWallsProvider(WallsProvider provider)
    {
        SetXY setWalls = ( model, x, y ) -> { model.setWalls(x,y, provider.getWalls(x,y)); };
        return createFromFunc( provider.getWidth(), provider.getHeight(), setWalls);
    }

    public HalfSquareRasterModel createFromMovesProvider(MovesProvider provider)
    {
        SetXY setMoves = ( model, x, y ) -> { model.setMoves(x,y,provider.getMoves(x,y)); };
        return createFromFunc( provider.getWidth(), provider.getHeight(), setMoves);
    }

}
