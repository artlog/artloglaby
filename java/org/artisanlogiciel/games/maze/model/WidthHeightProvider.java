package org.artisanlogiciel.games.maze.model;

public interface WidthHeightProvider {

    int getWidth();

    int getHeight();

}
