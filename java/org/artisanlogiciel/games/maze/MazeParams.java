package org.artisanlogiciel.games.maze;

import java.io.File;

/**
 * MazeParams contains parameters for a Maze generation
 **/
public interface MazeParams
{
    /** currently seed of java.util.random **/
    public long getSeed();

    public int getWidth();

    public int getHeight();

    public int getMaxDepth();

    /**
     * @return name of Maze, used for creating filename at saving time
     */
    public String getName();

    /**
     * @return directory owning where to save a maze
     */
    public File getSaveDir();

}
