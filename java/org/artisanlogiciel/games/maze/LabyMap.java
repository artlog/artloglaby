package org.artisanlogiciel.games.maze;

import java.util.LinkedList;

/**
 *  model with Bricks, based on walls
 */
public class LabyMap implements WallsProvider
{

    Brick[][] tileMap;
    LinkedList<Position> exits;

    public LabyMap(Brick[][] tileMap, LinkedList<Position> exits)
    {
        this.tileMap = tileMap;
        this.exits = exits;
    }

    Brick getAt(int x, int y)
    {
        return tileMap[x][y];
    }

    public short getWalls(int x, int y)
    {
        return getAt(x, y).getWalls();
    }

    public short getWalls(Position cell)
    {
        return getAt(cell.getX(), cell.getY()).getWalls();
    }

    public int getWidth()
    {
        return tileMap[0].length;
    }

    public int getHeight()
    {
        return tileMap.length;
    }

    public String toString()
    {
        int entryX = -1;
        int entryY = -1;
        for (Position exit : exits)
        {
            if (exit.getX() == -1)
            {
                entryY = exit.getY();
            }
            if (exit.getY() == -1)
            {
                entryX = exit.getX();
            }
        }
        String laby = "H";
        for (int x = 0; x < tileMap.length; x++)
        {
            if (entryX == x)
            {
                laby += " H";
            }
            else
            {
                laby += "HH";
            }
        }
        laby += "\n";
        for (int y = 0; y < tileMap[0].length; y++)
        {
            if (entryY == y)
            {
                laby += ">";
            }
            else
            {
                laby += "H";
            }
            for (int x = 0; x < tileMap.length; x++)
            {
                laby += tileMap[x][y].getUpString();
            }
            laby += "\n";
            laby += "H";
            for (int x = 0; x < tileMap.length; x++)
            {
                laby += tileMap[x][y].getLowString();
            }
            laby += "\n";
        }
        return laby;
    }
    // FIXME UGLY and not usable, need a better mapping, and not be a specialized toString...
    @Deprecated
    public String toShortString()
    {
        int entryX = -1;
        int entryY = -1;
        for (Position exit : exits)
        {
            if (exit.getX() == -1)
            {
                entryY = exit.getY();
            }
            if (exit.getY() == -1)
            {
                entryX = exit.getX();
            }
        }
        String laby = "";
        for (int y = 0; y < tileMap[0].length; y++)
        {
            for (int x = 0; x < tileMap.length; x++)
            {
                laby += "" + BrickTextMapping.getDefaultChar(tileMap[x][y]);
            }
            laby += "\n";
        }
        return laby;
    }


}
