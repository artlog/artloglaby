package org.artisanlogiciel.games.maze.persist;

import org.artisanlogiciel.games.maze.model.HalfSquareRasterModel;

import java.io.*;

public class HalfSquareRasterModelPersistRaw {
    private HalfSquareRasterModel model;

    private static byte HEADER[] = new byte[]{(byte) 'L', (byte) 'A', (byte) 'B', (byte) '1'};

    public HalfSquareRasterModelPersistRaw(HalfSquareRasterModel model)
    {
        this.model = model;
    }

    public HalfSquareRasterModelPersistRaw()
    {
        this.model = model;
    }


    public void streamOut(String pFormat, OutputStream pOut) throws IOException {
        if ((pFormat == null) || (pFormat.equals("sraw"))) {
            // first raw format, not smart.
            DataOutputStream dataOut = new DataOutputStream(pOut);
            // should dump this to stream.
            dataOut.write(HEADER);
            dataOut.writeInt(model.getWidth());
            dataOut.writeInt(model.getHeight());
            dataOut.flush();
            long[][] t = model.getLines();
            for (int y = 0; y < model.getHeight(); y++) {
                for (int x = 0; x < model.getLongs(); x++) {
                    dataOut.writeLong(t[y][x]);
                }
            }
            dataOut.flush();
        } else {
            throw new IOException("Format " + pFormat + " Not yet implemented.");
        }
    }


    public HalfSquareRasterModel parseInputStream(String pFormat, InputStream pIn) throws IOException {
        if ((pFormat == null) || (pFormat.equals("sraw"))) {
            // maxdepth is unset then unmodified
            byte[] header = new byte[4];
            DataInputStream in = new DataInputStream(pIn);
            in.read(header);
            int rwidth = in.readInt();
            int rheight = in.readInt();
            if ((rwidth > 0) && (rheight > 0)) {
                int width = rwidth;
                int height = rheight;
                model= new HalfSquareRasterModel(width, height);
                // SHOULD CHECK max width and max height...
                // CLEAR == 0 and array is initialized with 0s
                long[][] t = model.getLines();
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < model.getLongs(); x++) {
                        t[y][x] = in.readLong();
                    }
                }
                return model;
            } else {
                throw new IOException("Invalid header for width and height");
            }
            // should be at end of stream ? Not necessary can stream multiple
            // labs ( or tiling ).
        } else {
            throw new IOException("Format " + pFormat + " Not yet implemented.");
        }

    }

}
