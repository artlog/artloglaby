package org.artisanlogiciel.games.maze.persist;

import org.artisanlogiciel.games.maze.LabyModel;

import java.io.*;

/**
 * raw model with short table inner of LabyModel
 */
public class MazePersistRaw
{
    private static byte HEADER[] = new byte[]{(byte) 'L', (byte) 'A', (byte) 'B', (byte) '0'};

    private LabyModel model;

    public MazePersistRaw(LabyModel model)
    {
        this.model = model;
    }

    public MazePersistRaw()
    {
        this.model = model;
    }


    public void streamOut(String pFormat, OutputStream pOut) throws IOException {
        if ((pFormat == null) || (pFormat.equals("raw"))) {
            // first raw format, not smart.
            DataOutputStream dataOut = new DataOutputStream(pOut);
            // should dump this to stream.
            dataOut.write(HEADER);
            dataOut.writeInt(model.getWidth());
            dataOut.writeInt(model.getHeight());
            dataOut.flush();
            for (int y = 0; y < model.getHeight(); y++) {
                for (int x = 0; x < model.getWidth(); x++) {
                    dataOut.writeShort(model.getPath(x,y));
                }
            }
            dataOut.flush();
        } else {
            throw new IOException("Format " + pFormat + " Not yet implemented.");
        }
    }


    public LabyModel parseInputStream(String pFormat, InputStream pIn) throws IOException {
        if ((pFormat == null) || (pFormat.equals("raw"))) {
            // maxdepth is unset then unmodified
            byte[] header = new byte[4];
            DataInputStream in = new DataInputStream(pIn);
            in.read(header);
            // TODO check header == HEADER
            int rwidth = in.readInt();
            int rheight = in.readInt();
            if ((rwidth > 0) && (rheight > 0)) {
                int width = rwidth;
                int height = rheight;
                // SHOULD CHECK max width and max height...
                // CLEAR == 0 and array is initialized with 0s
                short[][] t = new short[width][height];
                for (int y = 0; y < height; y++) {
                    for (int x = 0; x < width; x++) {
                        t[x][y] = in.readShort();
                    }
                }
                model= new LabyModel(width, height, t);
                return model;
            } else {
                throw new IOException("Invalid header for width and height");
            }
            // should be at end of stream ? Not necessary can stream multiple
            // labs ( or tiling ).
        } else {
            throw new IOException("Format " + pFormat + " Not yet implemented.");
        }

    }

}
