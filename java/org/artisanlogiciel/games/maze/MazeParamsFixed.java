package org.artisanlogiciel.games.maze;

import java.io.File;

public class MazeParamsFixed implements MazeParams {
    long seed;
    int width;
    int height;
    int maxdepth;
    File labdir;
    String name;

    public MazeParamsFixed() {
    }

    public MazeParamsFixed(MazeParams params) {
        setParams(params.getSaveDir(), params.getWidth(), params.getHeight(), params.getMaxDepth());
    }

    public void setParams(File saveDir, int W, int H, int MD) {
        labdir = saveDir;
        width = W;
        height = H;
        maxdepth = MD;
    }

    public MazeParamsFixed(File saveDir, int W, int H, int MD, long seed) {
        name = null;
        setParams(saveDir, W, H, MD);
        this.seed = seed;
    }

    public long getSeed() {
        return seed;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getMaxDepth() {
        return maxdepth;
    }

    public void setName(String n) {
        name = n;
    }

    public String getName() {
        if (name == null) {
            name = "lab" + width + "x" + height;
        }
        return name;
    }

    public File getSaveDir() {
        return labdir;
    }
}
