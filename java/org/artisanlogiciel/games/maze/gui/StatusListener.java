package org.artisanlogiciel.games.maze.gui;

public interface StatusListener {

    void addStatus(String pStatus);

}
