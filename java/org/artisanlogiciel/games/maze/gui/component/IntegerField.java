package org.artisanlogiciel.games.maze.gui.component;

import org.artisanlogiciel.games.maze.gui.Display;
import org.artisanlogiciel.games.maze.gui.MazeDefault;

import javax.swing.*;

public class IntegerField
{
    JLabel mLabel;
    JTextField mTextField;
    long mDefaultValue;

    public IntegerField(String label, long defaultValue)
    {
        mDefaultValue = defaultValue;
        // DEPENDENCY
        mLabel = new JLabel(MazeDefault.labels.getString(label));
        mTextField = new JTextField("" + defaultValue);
    }

    public IntegerField(int defaultValue)
    {
        mDefaultValue = defaultValue;
        mLabel = new JLabel("");
        mTextField = new JTextField("" + defaultValue);
    }


    public JLabel getLabel()
    {
        return mLabel;
    }

    public JTextField getTextField()
    {
        return mTextField;
    }

    public int getValue()
    {
        try {
            return Integer.parseInt(mTextField.getText());
        }
        catch (Exception e)
        {
            mTextField.setText("" + mDefaultValue);
        }
        return (int) mDefaultValue;
    }

    public long getLongValue()
    {
        try {
            return Long.parseLong(mTextField.getText());
        }
        catch (Exception e)
        {
            mTextField.setText("" + mDefaultValue);
        }
        return (int) mDefaultValue;
    }


}
