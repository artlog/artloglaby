package org.artisanlogiciel.games.maze.gui.component;

import javax.swing.*;

public class Panel
extends JPanel {

    protected void addField(IntegerField pField)
    {
        add(pField.getLabel());
        add(pField.getTextField());
    }

}
