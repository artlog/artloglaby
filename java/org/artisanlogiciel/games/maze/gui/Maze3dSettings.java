package org.artisanlogiciel.games.maze.gui;

import org.artisanlogiciel.games.maze.gui.component.IntegerField;
import org.artisanlogiciel.games.maze.gui.component.Panel;
import org.artisanlogiciel.games.stl.Maze3dParams;

import javax.swing.*;

public class Maze3dSettings
    extends Panel
{
    // grid size
    IntegerField xl;
    IntegerField yl;
    IntegerField zl;

    IntegerField w;
    IntegerField lg;
    IntegerField hg;

    JCheckBox reverse;

    Maze3dParams params;

    public Maze3dSettings(Maze3dParams params) {
        super();
        this.params = params;
        createSettingsGui();
    }

            void createSettingsGui() {
            if (params != null) {

                IntegerField xl = new IntegerField("width",params.getXl());
                addField(xl);

                IntegerField zl = new IntegerField("height",params.getZl());
                addField(zl);

                IntegerField yl = new IntegerField("depth",params.getYl());
                addField(yl);

                reverse = new JCheckBox("reverse",params.isReverse());
                add(reverse);

                w = new IntegerField(params.getW());
                addField(w);

                // lowground hightground
                lg = new IntegerField(params.getLg());
                addField(lg);
                hg = new IntegerField(params.getHg());
                addField(hg);
            }
        }

        Maze3dParams createParams()
        {
            return new Maze3dParams(
                    xl.getValue(),
                    yl.getValue(),
                    zl.getValue(),
                    w.getValue(),
                    lg.getValue(),
                    hg.getValue(),
                    reverse.isSelected());
        }

}
