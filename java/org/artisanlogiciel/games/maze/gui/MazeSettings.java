package org.artisanlogiciel.games.maze.gui;

import org.artisanlogiciel.games.maze.MazeParams;
import org.artisanlogiciel.games.maze.MazeParamsFixed;
import org.artisanlogiciel.games.maze.gui.component.IntegerField;
import org.artisanlogiciel.games.maze.gui.component.Panel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.Random;

public class MazeSettings extends Panel {
    MazeParams params;

    IntegerField fieldWidth = null;
    IntegerField fieldHeight = null;
    IntegerField fieldDepth = null;
    IntegerField fieldSeed = null;
    JCheckBox onewaywallCB = null;

    // TODO set width and height and depth of maze with gui
    public MazeSettings(MazeParams params) {
        super();
        this.params = params;
        createSettingsGui();
    }

    void createSettingsGui() {

        MazeParams defaultParams = MazeDefault.getInstance().getParams();

        if (params != null) {

            final JSlider slider = new JSlider(1,
                    Math.max(
                            Math.max(defaultParams.getHeight(),defaultParams.getHeight()),
                            Math.max(defaultParams.getWidth(),params.getWidth())));

            slider.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    // settextWidthWallSize(slider.getValue());
                    String valueStr = "" + slider.getValue();
                    fieldWidth.getTextField().setText(valueStr);
                    fieldHeight.getTextField().setText(valueStr);
                }
            });

            add(slider);

            fieldWidth = new IntegerField("width",params.getWidth());
            addField(fieldWidth);

            fieldHeight = new IntegerField("height",params.getHeight());
            addField(fieldHeight);

            fieldDepth = new IntegerField("depth",params.getMaxDepth());
            addField(fieldDepth);

            fieldSeed = new IntegerField("seed", params.getSeed());
            addField(fieldSeed);

            onewaywallCB = new JCheckBox("one way wall", false);
            add(onewaywallCB);
        }
    }

    public MazeParams resetParams() {
        params = new MazeParamsFixed(params.getSaveDir(),
                fieldWidth.getValue(),
                fieldHeight.getValue(),
                fieldDepth.getValue(),
                new Random().nextLong()
        );
        fieldSeed.getTextField().setText("" + params.getSeed());
        return params;
    }

}
