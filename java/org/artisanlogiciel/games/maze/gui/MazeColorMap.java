package org.artisanlogiciel.games.maze.gui;

import java.awt.*;

public class MazeColorMap {
    public Color background;
    public Color wall;
    public Color path;
    public Color resolved_path;
    public Color goal;

    public MazeColorMap(Color background, Color wall, Color path, Color resolved_path, Color goal) {
        this.background = background;
        this.wall = wall;
        this.path = path;
        this.resolved_path = resolved_path;
        this.goal = goal;
    }
}
