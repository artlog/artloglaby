package org.artisanlogiciel.games.maze.gui;

import org.artisanlogiciel.games.maze.model.WidthHeightProvider;

import javax.swing.*;

public abstract class CellGridComponent
extends JComponent {

    MazeCellRenderer cp;

    MazeCellRenderer createCellRenderer(boolean hexagon, WidthHeightProvider model, WidthHeightProvider frame)
    {
        MazeCellRenderer cellRenderer = hexagon ?
                new HexagonCellRenderer(model, frame, 0, 0)
                : new MazeCellRenderer(model, frame, 0, 0);
        return cellRenderer;
    }
}
