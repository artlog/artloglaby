package org.artisanlogiciel.games.maze.gui;

import org.artisanlogiciel.games.maze.Brick;
import org.artisanlogiciel.games.maze.LabyModel;
import org.artisanlogiciel.games.maze.Position;
import org.artisanlogiciel.games.maze.model.WidthHeightProvider;
import org.artisanlogiciel.games.maze.solve.DirectionPosition;

import java.awt.*;

public class MazeCellRenderer {
    int offsetX; // x offset of upper corner left in pixels
    int offsetY; // y offset of upper corner left in pixers

    int mapWidth;
    int mapHeight;

    // computed see adaptTo
    double width; // width of one cell in pixels
    double height; // height of one cell in pixels

    public MazeCellRenderer(WidthHeightProvider model, WidthHeightProvider frame, int x, int y) {
        resetMazeWidthHeight(model);
        offsetX = x;
        offsetY = y;
        adaptTo(frame.getWidth(), frame.getHeight());
    }

    public void resetMazeWidthHeight(WidthHeightProvider model) {
        mapWidth = model.getWidth();
        mapHeight = model.getHeight();
    }

    public void adaptTo(double W, double H) {
        double w = (W - offsetX) / mapWidth;
        double h = (H - offsetY) / mapHeight;
        if (w < 5)
            w = 5;
        if (h < 5)
            h = 5;
        setCellSize(w, h);
    }

    // for a given (x,y) pixel return cell position.
    Position toMazeCoordinates(int x, int y) {
        int pX = (int) ((double) (x - getOffsetX()) / getWidth());
        int pY = (int) ((double) (y - getOffsetY()) / getHeight());
        return new Position(pX, pY);
    }

    public void setCellSize(double w, double h) {
        width = w;
        height = h;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public Dimension getDimension() {
        return new Dimension(offsetX + (int) (mapWidth * width),
                offsetY + (int) (mapHeight * height));
    }

    public Color getGreyedColor(Color color, int greylevel)
    {
        Color greyedColor = new Color(
                Math.max(0,color.getRed() - greylevel),
                Math.max(0,color.getGreen() - greylevel),
                Math.max(0,color.getBlue() - greylevel));
        return greyedColor;
    }
    public void drawBackground(Graphics g, Position cell, short walls) {
        int x = offsetX + (int) (cell.getX() * width);
        int y = offsetY + (int) (cell.getY() * height);
        Color savecolor = g.getColor();
        int greylevel = walls << 2;
        g.setColor(getGreyedColor(savecolor,greylevel));
        //g.fillRect(x+1,y+1,((int)width) -1,((int)height) - 1);
        g.fillRect(x,y,((int)width),((int)height));
    }
    public void drawWalls(Graphics g, Position cell, short walls) {
        int x = offsetX + (int) (cell.getX() * width);
        int y = offsetY + (int) (cell.getY() * height);
        if ((cell.getY() == 0) && ( Brick.isFlagSet( Brick.UP, walls)))
            g.drawLine(x, y, x + (int) width, y);
        if (Brick.isFlagSet( Brick.DOWN, walls))
            g.drawLine(x, y + (int) height, x + (int) width, y + (int) height);
        if (Brick.isFlagSet( Brick.RIGHT, walls))
            g.drawLine(x + (int) width, y, x + (int) width, y + (int) height);
        if ((cell.getX() == 0) && (Brick.isFlagSet( Brick.LEFT, walls)))
            g.drawLine(x, y, x, y + (int) height);
    }

    public void drawPath(Graphics g, DirectionPosition dp, int pX, int pY, int mX, int mY) {
        if (dp != null) {
            boolean arrow = true;
            Position dot = dp.getPosition();
            double h = getHeight();
            if ((dot.getX() >= pX) && (dot.getY() >= pY) && (dot.getX() < mX) && (dot.getY() < mY)) {
                int x = offsetX + (int) (dot.getX() * width);
                int y = offsetY + (int) (dot.getY() * h);
                short path = dp.getDirection();
                int xm = x + (int) (width / 2);
                int ym = y + (int) (h / 2);
                if (LabyModel.isFlagSet(path,LabyModel.HORIZONTAL)) {
                    if (LabyModel.isFlagSet(path, LabyModel.POSITIVE)) {
                        g.drawLine(xm, ym - (int) (h / 4), x + (int) width, ym);
                        if (arrow ) g.drawLine(xm, ym, x + (int) width, ym);
                        g.drawLine(xm, ym + (int) (h / 4), x + (int) width, ym);
                    }
                    // LEFT /_
                    if (LabyModel.isFlagSet(path,LabyModel.NEGATIVE)) {
                        g.drawLine(x, ym, xm, ym + (int) (h / 4));
                        if (arrow) g.drawLine(x, ym, xm, ym);
                        g.drawLine(x, ym, xm, ym - (int) (h / 4));
                    }
                }
                if (LabyModel.isFlagSet(path,LabyModel.VERTICAL)) {
                    if (LabyModel.isFlagSet(path    ,LabyModel.POSITIVE)) {
                        g.drawLine(xm + (int) (width / 4), ym, xm, y + (int) h);
                        if (arrow) g.drawLine(xm, ym, xm, y + (int) height);
                        g.drawLine(xm - (int) (width / 4), ym, xm, y + (int) h);
                    }
                    // UP |\
                    if (LabyModel.isFlagSet(path,LabyModel.NEGATIVE)) {
                        g.drawLine(xm - (int) (width / 4), ym, xm, y);
                        if (arrow) g.drawLine(xm, ym, xm, y);
                        g.drawLine(xm + (int) (width / 4), ym, xm, y);
                    }
                }
            }
        }
    }

    public void drawDot(Graphics g, Position dot, Position min, Position max) {
        double h = getHeight();
        double radius = (h > width) ? width : h;
        int a = (int) (radius / 4);
        if ((dot.getX() >= min.getX()) && (dot.getY() >= min.getY()) && (dot.getX() < max.getX()) && (dot.getY() < max.getY())) {
            int x = offsetX + (int) (dot.getX() * width);
            int y = offsetY + (int) (dot.getY() * h);
            int r2 = (int) radius ; //(int) ((radius * 3) / 4);
            g.drawOval(x, y, r2, r2);
        }
        /* why do we drwa anything at all ?
        else {
            int x = offsetX + (int) (min.getX() * width);
            int y = offsetY + (int) (min.getY() * h);
            g.drawLine(x + 1, y + 1, x + (int) width - 1, y + (int) h - 1);
        }
        */
    }

}
