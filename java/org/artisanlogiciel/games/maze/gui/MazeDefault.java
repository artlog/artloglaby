package org.artisanlogiciel.games.maze.gui;

import org.artisanlogiciel.games.maze.MazeParams;
import org.artisanlogiciel.games.maze.MazeParamsFixed;
import org.artisanlogiciel.util.UTF8Control;

import java.io.File;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * All defaults
 * Any Hardcoded value should end here
 *
 */
public class MazeDefault {

    public final static ResourceBundle labels = ResourceBundle.getBundle("LabelsBundle", Locale.getDefault(), Display.class.getClassLoader(), new UTF8Control());

    private final static MazeDefault instance;

    static {
        instance = new MazeDefault();
    }

    // HARDCODED Default
    private MazeParamsFixed mParams = new MazeParamsFixed(new File("lab"), 300, 300, 20,1024L);
    // private MazeCellParameters mazeCellParameters = new MazeCellParameters();

    // delay after which a draging to draw a continuous line is ended, a new line will then start.
    public static final long dragTimeout = 200;

    public static MazeDefault getInstance()
    {
        return instance;
    }

    public MazeParams getParams()
    {
        return mParams;
    }

    public File getXpmBackground()
    {
        return new File("lab/titou20.xpm");
    }
}

