package org.artisanlogiciel.games.maze.gui;

import org.artisanlogiciel.games.maze.Brick;
import org.artisanlogiciel.games.maze.Position;
import org.artisanlogiciel.games.maze.model.WidthHeightProvider;

import java.awt.*;

/**
 * draw cell as an hexagon
 * using a square model ( LEFT UP only).
 */

public class HexagonCellRenderer
extends MazeCellRenderer
{
    public final static int SUBCELL = 4;
    public final static int SUBCELLY = 3;

    public HexagonCellRenderer(WidthHeightProvider model, WidthHeightProvider frame, int x, int y) {
        super(model, frame, x, y);
    }

    void drawLine(Graphics g, Position ref, int x, int y, int a, int b) {
        g.drawLine(
                (int) ((ref.getX() + x) * width / SUBCELL), (int) ((ref.getY() + y) * height / SUBCELL),
                (int) ((ref.getX() + a) * width / SUBCELL),(int) ((ref.getY() + b) * height / SUBCELL));
    }

    @Override
    public void drawBackground(Graphics g, Position cell, short walls) {
        UV uv= new UV(cell);
        int x = offsetX + (int) (uv.getX() * width / SUBCELL);
        int y = offsetY + (int) (uv.getY() * height / SUBCELL);
        Color savecolor = g.getColor();
        int greylevel = walls << 2;
        g.setColor(getGreyedColor(savecolor,greylevel));
        //g.fillRect(x+1,y+1,((int)width) -1,((int)height) - 1);
        g.fillRect(x,y,((int)width),((int)height));
    }

    class UV
        extends Position
    {
        int u;
        int v;

        UV(Position p)
        {
            super(p);
            u=p.getX();
            v=p.getY();
            setX(u*4 + 2*(v%2));
            setY(3*v);
        }

    }

    @Override
    public void drawWalls(Graphics g, Position cell, short walls) {
        drawHalfNW(g, cell, walls);
    }

    void drawHalfNW(Graphics g, Position cell, short walls)
    {
        UV uv= new UV(cell);
        // NW : (0,1) - (2,0)
        if (Brick.isFlagSet( Brick.UP, walls))
            drawLine(g,uv, 0,1,2,0);
        // W : (0,1) - (0,3)
        if (Brick.isFlagSet( Brick.LEFT, walls))
            drawLine(g,uv,0,1,0,3);
        // NE : (2,0) - (4,1)
        // if (Brick.isFlagSet( Brick.NE, walls))
        drawLine(g,uv,2,0,4,1);
    }

    void drawHalfSW(Graphics g)
    {
        // SW : (0,3) - (2,4)
        // SE : (2,4) - (4,3)
        // E : (4,3) - (4,1)
    }

    @Override
    public double getHeight() {
        return height * SUBCELLY / SUBCELL;
    }
}
