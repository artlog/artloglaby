package org.artisanlogiciel.games.maze;

public class PositionWithDepth
extends Position
{
    private int depth;

    public PositionWithDepth(int x, int y, int depth)
    {
        super(x,y);
        this.depth = depth;
    }

    public int getDepth()
    {
        return depth;
    }

    public String toString()
    {
        return "(" + getX() + "," + getY() + ")" + "/" + depth;
    }

}
