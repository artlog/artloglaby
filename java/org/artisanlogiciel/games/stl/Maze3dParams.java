package org.artisanlogiciel.games.stl;

public class Maze3dParams {

    // grid size
    int xl;
    int yl;
    int zl;

    int w;
    int lg;
    int hg;

    Wall3d south;
    Wall3d west;
    Wall3d north;
    Wall3d east;
    Wall3d highGround;
    Wall3d lowGround;

    // reverse : means that resolved path will be HighGround actual making maze more difficult to play with
    boolean reverse;

    // default
    public Maze3dParams()
    {
        this(10,10,10, 1 , 1, 3, false);
    }

    // w width is wall thickness
    public Maze3dParams(int xl, int yl, int zl, int w, int lg, int hg, boolean reverse)
    {
        this.xl = xl;
        this.yl = yl;
        this.zl = zl;
        this.reverse = reverse;
        int h = zl;

        this.w = w;
        this.lg = lg;
        this.hg = hg;

        south = new Wall3d(xl, w, h, 0, 0, 0);
        west = new Wall3d(w, yl, h, 0, 0, 0);
        north = new Wall3d(xl, w, h, 0, yl, 0);
        east = new Wall3d(w, yl, h, xl, 0, 0);
        highGround = new Wall3d(xl, yl, hg, 0, 0, 0);
        lowGround = new Wall3d(xl, yl, lg, 0, 0, 0);
    }

    public boolean isReverse() {
        return reverse;
    }

    public int getXl() {
        return xl;
    }

    public int getYl() {
        return yl;
    }

    public int getZl() {
        return zl;
    }

    public Wall3d getSouth() {
        return south;
    }

    public Wall3d getWest() {
        return west;
    }

    public Wall3d getNorth() {
        return north;
    }

    public Wall3d getEast() {
        return east;
    }

    public Wall3d getHighGround() {
        return highGround;
    }

    public Wall3d getLowGround() {
        return lowGround;
    }

    public int getLg() {
        return lg;
    }

    public int getHg() {
        return hg;
    }

    public int getW() {
        return w;
    }


}
