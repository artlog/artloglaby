package org.artisanlogiciel.games.minetest;

/**
 * generate directly lua maps text ( no intermediate LuaObject ).
 */
public class WorlEditGenerator
{

    Node refNode;
    StringBuilder luaNode;
    boolean start = true;

    public WorlEditGenerator(StringBuilder luaNode, Node refNode) {
        this.luaNode = luaNode;
        start = true;
        this.refNode = refNode;
    }

    private void addIntMember(String name, int value)
    {
        addSeparator();
        addMember(name);
        luaNode.append(value);
    }

    private void addStringMember( String name, String value)
    {
        addSeparator();
        addMember(name);
        luaNode.append('"').append(value).append('"');
    }

    private void addMember(String name) {
        luaNode.append("[\"").append(name).append("\"]=");
    }

    private void addSeparator() {
        if ( ! start)
        {
            luaNode.append(",");
        }
        start = false;
    }

    public void writeStart()
    {
        luaNode.append("5.return {");
    }
    public void writeNode(Node node)
    {
        addSeparator();
        luaNode.append("{");
        start=true;
        addIntMember("x", node.getX() - refNode.getX());
        addIntMember("y", node.getY() - refNode.getY());
        addIntMember("z", node.getZ() - refNode.getZ());
        addStringMember("name",node.getMaterial().toString() );
        luaNode.append("}");
        start=false;
    }

    public void writeEnd()
    {
        luaNode.append("}");
    }

}
