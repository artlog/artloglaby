package org.artisanlogiciel.games.minetest;

import java.util.HashMap;

public class Node {
    // could be a core.v3s16
    int x;
    int y;
    int z;
    Material material;

    public Node(int x, int y, int z, Material material) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.material = material;
    }

    public Node(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.material = Material.DEFAULT;
    }

    public Node(HashMap<String, Object> map)
            throws ClassCastException, NullPointerException
    {
        // FIXME WARNING HACK reverse x and z ...
        x = ((Integer) map.get("x")).intValue();
        y = ((Integer) map.get("z")).intValue();
        z = ((Integer) map.get("y")).intValue();
        material = Material.getMaterialByName( (String) map.get("name"));
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Node{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", material=" + material +
                '}';
    }
}
