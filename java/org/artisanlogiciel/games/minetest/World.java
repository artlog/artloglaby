package org.artisanlogiciel.games.minetest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class World
    extends Range
{

    List<Node> nodes;

    // z range
    HashMap<Integer,Slice> zSlices;

    public World()
    {
        super();
        nodes = new ArrayList<>();
        zSlices = new HashMap<>();
    }

    public void addNode(Node node)
    {
        nodes.add(node);
        addNodeInSlice(new Integer(node.getZ()),node);
    }

    private void addNodeInSlice(Integer z, Node node) {
        updateBounds(z.intValue());
        Slice s = zSlices.get(z);
        Integer x = new Integer(node.getX());
        if ( s == null )
        {
            s = new Slice();
            zSlices.put(z,s);
        }
        s.addNodeInRaw(x,node);

    }

    public void addList(List<Object> objectList)
    {
        for (Object o : objectList)
        {
            if ( o instanceof HashMap )
            {
                HashMap<String,Object> map = (HashMap<String,Object>) o;
                Node node = new Node(map);
                addNode(node);
            }
        }
    }

    public Node getNode(int x, int y, int z)
    {
        Slice s = zSlices.get(new Integer(z));
        if ( s == null )
        {
            return null;
        }
        else
        {
            return s.getNode(x,y);
        }
    }

    public Slice getSlice(int z)
    {
        return zSlices.get(new Integer(z));
    }

    @Override
    public String toString() {
        return "World{" +
                "nodes=" + nodes +
                '}';
    }
}
