package org.artisanlogiciel.games.minetest;

import org.artisanlogiciel.lua.CharProvider;
import org.artisanlogiciel.lua.LuaObject;
import org.artisanlogiciel.lua.Parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class WorldEditReader {

    InputStream input;

    public WorldEditReader(InputStream pIn)
    {
        input = pIn;
    }

    public World read()
            throws IOException
    {
        World world = new World();


        BufferedReader in = new BufferedReader(new InputStreamReader(input));
        byte b[] = new byte[128*1024];
        input.read(b);
        String contents = new String(b);
        in.close();

        System.out.println(contents);

        CharProvider reader = new CharProvider(contents);
        Parser parser = new Parser(reader);
        LuaObject result = parser.parse();
        if (result != null) {
            System.out.println(result.toString());
            Object we = result.wrapToJava();
            System.out.println(we);
            world.addList( (List<Object>) we);
            System.out.println(world);
        } else {
            System.err.println("result null");
        }
        return world;
    }

}
