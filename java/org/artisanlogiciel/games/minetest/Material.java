package org.artisanlogiciel.games.minetest;

public class Material {

    public static final String GRASS_MATERIAL = "default:dirt_with_grass";

    public static Material DEFAULT = new Material(GRASS_MATERIAL);

    private String name;

    public Material(String name) {
        this.name = name;
    }

    public static Material getMaterialByName(String name)
    {
        return DEFAULT;
    }

    @Override
    public String toString() {
        return name;
    }
}
