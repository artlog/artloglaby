package org.artisanlogiciel.games.minetest;

import java.util.HashMap;

public class Slice
    extends Range
{

    // x is index
    HashMap<Integer, Raw> raws;

    public Range getRawRange() {
        return rawRange;
    }

    Range rawRange;

    public Slice() {
        super();
        raws = new HashMap<>();
        rawRange = new Range();
    }

    public void addNodeInRaw(Integer x, Node node) {
        updateBounds(x.intValue());
        Raw r = raws.get(x);
        if ( r == null )
        {
            r = new Raw();
            raws.put(x,r);
        }
        r.addNode(new Integer(node.getY()), node);
        rawRange.union(r);
    }

    public Node getNode(int x, int y)
    {
        Raw r = raws.get(x);
        if ( r == null )
        {
            return null;
        }
        return r.getNode(y);
    }

    public Raw getRaw(Integer x)
    {
        return raws.get(x);
    }
}
