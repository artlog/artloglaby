package org.artisanlogiciel.games.minetest;

/**
 * [min,max] range with bounds included.
 */
public class Range {

    // included
    int min;
    // included
    int max;

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public Range()
    {
        // invalid range, means empty interval
        min = 0;
        max = -1;
    }

    public Range(int pMin, int pMax)
    {
        min = pMin;
        max = pMax;
    }

    public boolean isEmpty()
    {
        return ( min > max );
    }

    boolean union(Range other)
    {
        boolean update = false;
        if ( ! other.isEmpty()) {
            update = updateBounds(other.getMin()) | updateBounds(other.getMax());
        }
        return update;
    }

    protected boolean updateBounds(int v)
    {
        boolean update = false;

        // special case where previous range was unset ( min > max ).
        if ( isEmpty() )
        {
            min = v;
            max = v;
            return true;
        }
        if ( v < min )
        {
            update = true;
            min = v;
        }
        if ( v > max )
        {
            update = true;
            max = v;
        }
        return update;
    }


    public int getRangeSize() {
        if (isEmpty())
        {
            return 0;
        }
        else
        {
            return max - min + 1;
        }
    }
}
