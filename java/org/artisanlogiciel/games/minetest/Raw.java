package org.artisanlogiciel.games.minetest;

import java.util.HashMap;

public class Raw
extends  Range {
    // y is index
    public HashMap<Integer,Node> nodes;

    public Raw() {
        super();
        nodes = new HashMap<>();
    }

    public void addNode(Integer y, Node node)
    {
        updateBounds(y.intValue());
        nodes.put(y,node);
    }

    public Node getNode( int y)
    {
        return nodes.get(new Integer(y));
    }
}
