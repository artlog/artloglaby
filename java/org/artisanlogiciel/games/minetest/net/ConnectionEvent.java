package org.artisanlogiciel.games.minetest.net;

public class ConnectionEvent {

    ConnectionEventType m_event_type;
    int m_peer_id;
    byte[] m_data;
    boolean m_timeout;


    ConnectionEvent(ConnectionEventType eventType)
    {
        m_event_type = eventType;
    }

    String describe()
    {
        return m_event_type.toString();
    }

    ConnectionEventPtr create(ConnectionEventType type)
    {
        return null;
    }

	ConnectionEventPtr dataReceived(int peer_id, byte[] data)
    {
        return null;
    }

    ConnectionEventPtr peerAdded(int peer_id, Address address)
    {
        return null;
    }

    ConnectionEventPtr peerRemoved(int peer_id, boolean is_timeout, Address address)
    {
        return null;
    }

	ConnectionEventPtr bindFailed()
    {
        return null;
    }
}
