package org.artisanlogiciel.games.minetest.net;

import org.artisanlogiciel.games.minetest.core.PacketException;
import org.artisanlogiciel.games.minetest.core.Serialize;

public class BufferedPacket {

    public static int BASE_HEADER_SIZE = 7;

    // Data of the packet, including headers
    byte[] m_data;

    BufferedPacket(byte[] data)
    {
        m_data = data;
    }

    int getSeqNum()
            throws PacketException
    {
        return Serialize.readU16(m_data, BASE_HEADER_SIZE + 1, size());
    }

    int size()
    {
        return m_data.length;
    }
}
