package org.artisanlogiciel.games.minetest.net;

import org.artisanlogiciel.games.minetest.core.PacketException;

import java.util.LinkedList;
import java.util.List;

public class ReliablePacketBuffer {

    List<BufferedPacket> m_list = new LinkedList<>();

    void insert(BufferedPacket packet, int nextExpected)
    throws PacketException
    {
        //
        int seqNum = packet.getSeqNum();
        m_list.add(packet);
    }

}
