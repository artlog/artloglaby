package org.artisanlogiciel.games.minetest.net;

public enum PacketType {
    PACKET_TYPE_CONTROL,
    PACKET_TYPE_ORIGINAL,
    PACKET_TYPE_SPLIT,
    PACKET_TYPE_RELIABLE,
    PACKET_TYPE_ERROR;

    static PacketType getPacketType(int value)
    {
        switch (value)
        {
            case 0:
                return PACKET_TYPE_CONTROL;
            case 1:
                return PACKET_TYPE_ORIGINAL;
            case 2:
                return PACKET_TYPE_SPLIT;
            case 3:
                return PACKET_TYPE_RELIABLE;
            default:
                return PACKET_TYPE_ERROR;
        }
    }

}
