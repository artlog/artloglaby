package org.artisanlogiciel.games.minetest.net;

public enum ConnectionEventType {
    CONNEVENT_NONE,
	CONNEVENT_DATA_RECEIVED,
	CONNEVENT_PEER_ADDED,
	CONNEVENT_PEER_REMOVED,
	CONNEVENT_BIND_FAILED
}
