package org.artisanlogiciel.games.minetest.core;

public class PacketException
extends Exception
{
    public PacketException(String message)
    {
        super(message);
    }
}
