package org.artisanlogiciel.osm;

/**
 * https://wiki.openstreetmap.org/wiki/Node
 */
public class Node {
    NodeRef ref;
    double lat;
    double lon;

    public NodeRef getRef() {
        return ref;
    }

    public Node(NodeRef ref, double lat, double lon) {
        this.ref = ref;
        ref.setNode(this);
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String toString()
    {
        return "(id=" + ref.id + " lon=" + lon + " lat=" + lat + ")";
    }
}
