package org.artisanlogiciel.osm;

import java.util.List;

public class Way {
    long id;

    public Way(long id) {
        this.id = id;
    }

    public Way(long id, List<NodeRef> ndlist) {
        this.id = id;
        this.ndlist = ndlist;
    }

    public List<NodeRef> getNdlist() {
        return ndlist;
    }

    List<NodeRef> ndlist;
}
