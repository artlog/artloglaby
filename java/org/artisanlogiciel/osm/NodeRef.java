package org.artisanlogiciel.osm;

public class NodeRef {
    long id;
    Node node;

    public NodeRef(long id) {
        this.id = id;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Node getNode()
    {
        return node;
    }

    public String toString()
    {
        return node != null ? node.toString(): "nd:" + id;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(id);
    }

    @Override
    public boolean equals(Object o) {
        if ( o instanceof NodeRef ) {
            return ((NodeRef) o).id == id;
        }
        return false;
    }
}
