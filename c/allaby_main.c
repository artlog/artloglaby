#include "al_options.h"

// NOT YET IMPLEMENTED
int main(int argc, char ** argv)
{
  int exitcode = 1;
  struct al_options * options = al_options_create(argc,argv);
  al_options_set_debug(options,0);
  struct alhash_datablock * debugdata = al_option_get(options,"debug");
  if ( debugdata != NULL )
    {
      exitcode = 0;
    }
  al_options_release(options);
  
  return exitcode;
}
