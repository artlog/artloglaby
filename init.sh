#!/bin/bash

if [[ ! -d  artlog_toolbox ]]
then
    git clone https://github.com/artlog/artlog_toolbox.git artlog_toolbox
fi
pushd artlog_toolbox
git checkout master
popd
artlog_toolbox/deploy.sh
./doit.sh
